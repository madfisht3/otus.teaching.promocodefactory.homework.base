﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }

        public Task AddAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data.Add(entity);
            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult<IEnumerable<T>>(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> UpdateAsync(T entity)
        {
            var entityToUpdate = Data.SingleOrDefault(e => e.Id == entity.Id);
            if (entityToUpdate == null) return Task.FromResult(false);

            var index = Data.IndexOf(entityToUpdate);
            Data[index] = entity;
            return Task.FromResult(true);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            var entityToUpdate = Data.SingleOrDefault(e => e.Id == id);
            if (entityToUpdate == null) return Task.FromResult(false);

            var index = Data.IndexOf(entityToUpdate);
            Data.RemoveAt(index);
            return Task.FromResult(true);
        }

        protected List<T> Data { get; set; }
    }
}