﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Добавить нового сотрудника.
        /// </summary>
        /// <param name="employee">Данные нового сотрудника.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddEmployeeAsync(NewEmployeeRequest employee)
        {
            if (employee == null || !employee.Validate()) return BadRequest();

            var availableRoles = await _rolesRepository.GetAllAsync();
            if (!employee.Roles.All(r => availableRoles.Any(a => a.Id == r))) return BadRequest();
            
            await _employeeRepository.AddAsync(new Employee
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
                Roles = employee.Roles.Select(r => availableRoles.Single(a => a.Id == r)).ToList()
            });
            return Ok();
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Обновить данные сотрудника.
        /// </summary>
        /// <param name="employee">Новые данные сотрудника.</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync(UpdatedEmployeeRequest employee)
        {
            if (employee == null || !employee.Validate()) return BadRequest();

            var availableRoles = await _rolesRepository.GetAllAsync();
            if (!employee.Roles.All(r => availableRoles.Any(a => a.Id == r))) return BadRequest();

            var result = await _employeeRepository.UpdateAsync(new Employee
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount,
                Roles = employee.Roles.Select(r => availableRoles.Single(a => a.Id == r)).ToList()
            });

            if (result)
                return Ok();
            else
                return NotFound();
        }

        /// <summary>
        /// Удалить сотрудника по Id.
        /// </summary>
        /// <param name="id">Id удаляемого сотрудника.</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            var result = await _employeeRepository.DeleteByIdAsync(id);

            if (result)
                return Ok();
            else
                return NotFound();
        }
    }
}