﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Сущность для операций обновления существующего сотрудника.
    /// </summary>
    public class UpdatedEmployeeRequest
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }

        public List<Guid> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public bool Validate()
        {
            return !string.IsNullOrEmpty(FirstName)
                && !string.IsNullOrEmpty(LastName)
                && !string.IsNullOrEmpty(Email)
                && Roles != null && Roles.Count > 0
                && AppliedPromocodesCount >= 0;
        }
    }
}
